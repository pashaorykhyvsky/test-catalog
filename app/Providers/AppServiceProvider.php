<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Maatwebsite\Excel\Imports\HeadingRowFormatter;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $headings = config('imports.xlsx_products.headings');
        HeadingRowFormatter::extend('custom', function ($value) use ($headings) {
            return $headings[$value] ?? $value;
        });
        HeadingRowFormatter::default('custom');
    }
}
