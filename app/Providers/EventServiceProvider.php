<?php

namespace App\Providers;

use App\Events\ProductsImportCompleted;
use App\Listeners\ProductsImportCleaning;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        ProductsImportCompleted::class => [
            ProductsImportCleaning::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
