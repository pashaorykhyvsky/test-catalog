<?php

namespace App\Rules;

use Illuminate\Support\Facades\Redis;
use Illuminate\Contracts\Validation\Rule;

class NoDuplicates implements Rule
{
    private $uuid;

    /**
     * Create a new rule instance.
     *
     * @param string $uuid
     */
    public function __construct(string $uuid)
    {
        $this->uuid = $uuid;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  string  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return Redis::command('sadd', ["imports:{$this->uuid}:row_identifiers", $value]);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'duplicated value';
    }
}
