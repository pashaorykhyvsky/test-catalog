<?php

namespace App\Imports;

use App\Events\ProductsImportFailure;
use App\Models\Product;
use App\Events\ProductsImportCompleted;
use App\Rules\NoDuplicates;
use Maatwebsite\Excel\Events\AfterImport;
use Maatwebsite\Excel\Validators\Failure;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Illuminate\Support\Facades\Cache;
use Illuminate\Contracts\Queue\ShouldQueue;

class ProductsImport implements WithBatchInserts,
                                WithChunkReading,
                                WithValidation,
                                ToModel,
                                WithMapping,
                                SkipsOnFailure,
                                WithEvents,
                                WithHeadingRow,
                                ShouldQueue
{
    use RegistersEventListeners;

    /**
     * @var array
     */
    protected $dictionary;

    /**
     * @var string
     */
    protected $uuid;

    /**
     * ProductImport constructor.
     * @param string $uuid
     * @param array $dictionary
     */
    public function __construct(string $uuid, array $dictionary)
    {
        $this->uuid = $uuid;
        $this->dictionary = $dictionary;
    }

    /**
     * @return int
     */
    public function batchSize(): int
    {
        return 500;
    }

    /**
     * @return int
     */
    public function chunkSize(): int
    {
        return 500;
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'code' => ['required', 'string', new NoDuplicates($this->uuid)],
            'in_stock' => 'boolean|nullable',
            'warranty' => 'nullable|integer',
            'description' => 'required|string',
            'name' => 'required|string|max:255',
            'retail_price' => 'required|integer',
            'category' => 'required|string|max:255',
            'rubric_one' => 'nullable|string|max:255',
            'rubric_two' => 'nullable|string|max:255',
            'manufacturer' => 'required|string|max:255',
        ];
    }

    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Model[]|null
     */
    public function model(array $row)
    {
        Cache::increment("imports:{$this->uuid}:inserted_count");
        return new Product($row);
    }

    /**
     * @param mixed $row
     *
     * @return array
     */
    public function map($row): array
    {
        if (is_string($row['in_stock'])) {
            $inStock = mb_strtolower($row['in_stock']);
            if ($inStock === $this->dictionary['in_stock']['yes']) {
                $row['in_stock'] = true;
            } elseif ($inStock === $this->dictionary['in_stock']['no']){
                $row['in_stock'] = false;
            }
        }

        if (is_string($row['warranty']) &&
            mb_strtolower($row['warranty']) === $this->dictionary['warranty']['no']){
            $row['warranty'] = null;
        }

        return $row;
    }

    /**
     * @param Failure[] $failures
     */
    public function onFailure(Failure ...$failures)
    {
        Cache::increment("imports:{$this->uuid}:failed_count", count($failures));
        array_walk($failures, function (&$value) {
            /** @var Failure $value */
            $value = $value->toArray();
        });
        event(new ProductsImportFailure($failures));
    }

    public static function afterImport(AfterImport $event)
    {
        /** @var self $concrete */
        $concrete = $event->getConcernable();

        event(new ProductsImportCompleted(
            $concrete->uuid,
            Cache::get("imports:{$concrete->uuid}:inserted_count"),
            Cache::get("imports:{$concrete->uuid}:failed_count")
        ));

        Cache::forget("imports:{$concrete->uuid}:failed_count");
        Cache::forget("imports:{$concrete->uuid}:inserted_count");
    }
}
