<?php

namespace App\Services;

use Illuminate\Support\Str;
use App\Imports\ProductsImport;
use Maatwebsite\Excel\Facades\Excel;

class ProductService
{
    public function import(string $filePath): void
    {
        $import = new ProductsImport(
            Str::uuid()->toString(),
            config('imports.xlsx_products')
        );

        Excel::import($import, $filePath);
    }
}
