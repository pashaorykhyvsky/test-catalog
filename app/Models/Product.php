<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';

    public $timestamps = false;

    protected $fillable = [
        'code',
        'name',
        'category',
        'warranty',
        'in_stock',
        'rubric_one',
        'rubric_two',
        'description',
        'manufacturer',
        'retail_price',
    ];
}
