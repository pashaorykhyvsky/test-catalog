<?php

namespace App\Http\Controllers\Api;

use App\Services\ProductService;
use App\Http\Requests\Product\XlsxImportRequest;

class ProductController extends ApiController
{
    /**
     * @var ProductService
     */
    private $productService;

    public function __construct(ProductService $productService)
    {
        $this->productService = $productService;
    }

    public function xlsxImport(XlsxImportRequest $request)
    {
        $filePath = $request->file('import')->store('imports');

        $this->productService->import($filePath);

        return response()->json(['message' => 'OK']);
    }
}
