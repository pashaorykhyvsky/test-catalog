<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('rubric_one')->nullable();
            $table->string('rubric_two')->nullable();
            $table->string('category');
            $table->string('manufacturer');
            $table->string('name');
            $table->text('description');
            $table->integer('retail_price');
            $table->smallInteger('warranty')->nullable();
            $table->string('code');
            $table->boolean('in_stock')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
