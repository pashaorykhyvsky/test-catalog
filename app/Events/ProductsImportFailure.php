<?php

namespace App\Events;

use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;

class ProductsImportFailure
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    /**
     * @var array
     */
    private $failures;

    /**
     * Create a new event instance.
     *
     * @param array $failures
     */
    public function __construct(array $failures)
    {
        $this->failures = $failures;
    }

    /**
     * @return array
     */
    public function broadcastWith(): array
    {
        return [
            'data' => [
                $this->failures,
            ]
        ];
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('imports');
    }
}
