<?php

namespace App\Listeners;

use App\Events\ProductsImportCompleted;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;

class ProductsImportCleaning
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ProductsImportCompleted  $event
     * @return void
     */
    public function handle($event)
    {
        Redis::command('del', [
            "imports:{$event->uuid}:row_identifiers",
        ]);
    }
}
