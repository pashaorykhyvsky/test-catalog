<?php

namespace App\Http\Requests\Product;

use App\Rules\XlsxHeadings;
use App\Rules\UploadMaxFileSize;
use Illuminate\Foundation\Http\FormRequest;

class XlsxImportRequest extends FormRequest
{
    protected $dictionary;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'import' => [
                'required',
                'mimes:xlsx,xls',
                new XlsxHeadings(config('imports.xlsx_products')['headings']),
                new UploadMaxFileSize(),
            ]
        ];
    }
}
