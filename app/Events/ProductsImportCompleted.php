<?php

namespace App\Events;

use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;

class ProductsImportCompleted
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var int
     */
    public $inserted;

    /**
     * @var int
     */
    public $failed;

    /**
     * @var string
     */
    public $uuid;

    /**
     * Create a new event instance.
     *
     * @param string $uuid
     * @param int $inserted
     * @param int $failed
     */
    public function __construct(string $uuid, int $inserted, int $failed)
    {
        $this->inserted = $inserted;
        $this->failed = $failed;
        $this->uuid = $uuid;
    }

    /**
     * @return array
     */
    public function broadcastWith(): array
    {
        return [
            'data' => [
                'inserted' => $this->inserted,
                'failed' => $this->failed,
            ]
        ];
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('imports');
    }
}
