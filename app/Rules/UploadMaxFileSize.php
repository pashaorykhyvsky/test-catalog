<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Http\UploadedFile;

class UploadMaxFileSize implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  UploadedFile  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return $value->getSize() < $this->parseBytes(ini_get('upload_max_filesize'));
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'server max file size exceeded';
    }

    protected function parseBytes($str): int
    {
        $str = strtolower(trim($str));
        $last = $str[strlen($str)-1];
        $val = (double)$str;
        switch($last) {
            case 'g':
                $val *= 1024 ** 3;
                break;
            case 'm':
                $val *= 1024 ** 2;
                break;
            case 'k':
                $val *= 1024;
                break;
        }
        return $val;
    }
}
