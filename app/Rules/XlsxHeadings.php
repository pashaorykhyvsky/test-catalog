<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\HeadingRowImport;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class XlsxHeadings implements Rule
{
    /**
     * @var array
     */
    private $headings;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct(array $headings)
    {
        $this->headings = $headings;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  UploadedFile  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $headings = (new HeadingRowImport())->toArray($value);
        return empty(array_diff($this->headings, $headings[0][0] ?? []));
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The validation error message.';
    }
}
